class Snake{
  
  int x, y, lastX, lastY, snakeLength;
  int foodX, foodY;
  Board myBoard;
  ArrayList<int[]> positions = new ArrayList(); 
  boolean dead = false;
  int deadCount = 0;
  
  Snake(Board board){
    x = 0;
    y = 0;
    lastX = 0;
    lastY = 0;
    snakeLength = 1;
    myBoard = board;
    newFood();
  }
  
  void moveUp(){
    lastX = x;
    lastY = y;
    int[] temp = {lastX,lastY};
    positions.add(temp);
    if(y > 0){
      y--;
    }else{
      y = myBoard.boardHeight - 1;
    }
    updateSnake();
  }
  
  void moveLeft(){
    lastX = x;
    lastY = y;
    int[] temp = {lastX,lastY};
    positions.add(temp);
    if(x > 0){
      x--;
    }else{
      x = myBoard.boardWidth - 1;
    }
     updateSnake();
  }
  
  void moveDown(){
    lastX = x;
    lastY = y;
     int[] temp = {lastX,lastY};
    positions.add(temp);
    if(y < myBoard.boardHeight -1){
      y++;
    }else{
      y = 0;
    }
     updateSnake();
  }
  
  void moveRight(){
    lastX = x;
    lastY = y;
     int[] temp = {lastX,lastY};
    positions.add(temp);
     if(x < myBoard.boardWidth - 1){
      x++;
    }else{
      x = 0;
    }
     updateSnake();
  }
  
  void setLength(int size){
    snakeLength = size;
  }
  
  int getLength(){
   return snakeLength; 
  }
  
  void updateSnake(){
    myBoard.update(x,y,1);
    while(positions.size() >= snakeLength){
      myBoard.update(positions.get(0)[0], positions.get(0)[1], 3);
      positions.remove(0);
    }
    
    checkCollision();
    
    
  }
  
  void checkFood(){
    if(x == foodX && y == foodY){
      snakeLength++;
      newFood();
    }
  }
  
  void newFood(){
    foodX = (int)random(0, myBoard.boardWidth);
    foodY = (int)random(0, myBoard.boardHeight);
    if(foodX == x && foodY == y){
      println("maybe fixed food bug");
      newFood();
    }
    for(int count = 0; count < positions.size(); count++){
      if(foodX == positions.get(count)[0] && foodY == positions.get(count)[1] ){
       newFood();
       println("Re-generating!!");
      }
    } 
    if(foodX == lastX && foodY == lastY){
      newFood();
      println("might have fixed Bug");
    }
    
    myBoard.update(foodX,foodY,2);
  }
  
  void checkCollision(){
      for(int count = 0; count < positions.size(); count++){
        if(x == positions.get(count)[0] && y == positions.get(count)[1]){
          //x = 0;
          //y = 0;
          //snakeLength = 1;
          dead = true;
          println("Killing snake");
          deadCount = positions.size()-2;
          myBoard.update(lastX, lastY, 5);
          
          //updateSnake();
        }
      }
    checkFood();
  }
  
  void deathAnimation(){
    println("snake is dead: " + dead + ", and deadCount is at: " + deadCount);
    if(deadCount >= 0 && dead){
      myBoard.update(positions.get(deadCount)[0], positions.get(deadCount)[1], 6);
      if(deadCount > 0){
        deadCount--;
      }
      else{
        deadCount = -1; 
      }
      
    }else{
      println("deleting snake body");
      for(int count = positions.size()-1; count > 0; count--){
        myBoard.update(positions.get(count)[0], positions.get(count)[1], 3);
        positions.remove(count);
      }
      x = 0;
      y = 0;
      snakeLength = 1;
      dead = false;
      println("is snake still dead?: " + dead);
    }
  }
  
}
