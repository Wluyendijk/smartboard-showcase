import http.requests.*;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Date;
import java.text.SimpleDateFormat;

class MyThread implements Runnable{
  private GetRequest get;
  String gameID;
  
  public MyThread( String id){
    
    gameID = id;
    //println("Requesting data...");
    
    get = new GetRequest("https://lichess.org/api/board/game/stream/" + GameID);
    get.addHeader("Authorization","Bearer YlslUaE0uXn1stzS");
  }
  
  public void run(){
    try{
      get.send();
      Thread.sleep(100);
    }catch(InterruptedException ie){
      
    }
  }
  
  public String getBoardState(){
     return get.getContent(); 
  }
  
  public void makeMove(){
    
  }
  
  public void updateBoard(){
    this.run();
  }
  
  public void getResponse(){
   String updatedBoard = getBoardState();
    if(updatedBoard != null){
     String[] objectResponses = {"", ""};
     objectResponses = updatedBoard.split("\n");
   
      JSONObject response;
      response = parseJSONObject(objectResponses[objectResponses.length-1]);
      String responseMoves = response.getString("moves");
      if(responseMoves != null){
          String[] moveArray = responseMoves.split(" ");
          String opponentsMove = moveArray[moveArray.length-1];
          println("Opponent's move: " + opponentsMove);
          
          String moveOne = opponentsMove.substring(0,2);
          String moveTwo = opponentsMove.substring(2);
          println("From: " + moveOne);
          println("To: " + moveTwo);
          if(!moveOne.equals(moves[0]) || !moveTwo.equals(moves[1])){
            makeUpdatedMove(moveOne, moveTwo);
           println("made Move");
          }
          else{
            println(moveOne + " = " + moves[0] + " && " + moveTwo + " = " + moves[1]);
            delay(5000);
            sendMove = true;
            getResponse();
          }
      }else{//Check game status
        //println(updatedBoard);
        response = parseJSONObject(updatedBoard);
        println(updatedBoard);
        JSONObject gameState = response.getJSONObject("state");
        if(!gameState.getString("status").equals("started")){
          println("Winner is: " + gameState.getString("winner"));
          if(gameState.getString("winner").equals("white")){
            ChessPiece opponentsKing = pieces.get("BK");
            chess.chessBoard.update((int)opponentsKing.position[0]-1, (int)opponentsKing.position[1]-1, 2);
            ChessPiece myKing = pieces.get("WK");
            chess.chessBoard.update((int)myKing.position[0]-1, (int)myKing.position[1]-1, 7);
          }else if(gameState.getString("winner").equals("black")){
            ChessPiece opponentsKing = pieces.get("BK");
            chess.chessBoard.update((int)opponentsKing.position[0]-1, (int)opponentsKing.position[1]-1, 7);
            ChessPiece myKing = pieces.get("WK");
            chess.chessBoard.update((int)myKing.position[0]-1, (int)myKing.position[1]-1, 6);
          }else{
           println("UH OH SOMETHING WENT WRONG..."); 
          }
        }else{
             // response;
        response = parseJSONObject(objectResponses[objectResponses.length-1]);
        responseMoves = response.getString("moves");
        if(responseMoves != null){
            String[] moveArray = responseMoves.split(" ");
            String opponentsMove = moveArray[moveArray.length-1];
            println("Opponent's move: " + opponentsMove);
            
            String moveOne = opponentsMove.substring(0,2);
            String moveTwo = opponentsMove.substring(2);
            println("From: " + moveOne);
            println("To: " + moveTwo);
            if(!moveOne.equals(moves[0]) || !moveTwo.equals(moves[1])){
              makeUpdatedMove(moveOne, moveTwo);
             println("made Move");
            }
            
             sendMove = false; 
             yourTurn = true;
      }
        }
          //No moves availible...
        }
      }else{
       println("response took too long trying again in 5 seconds");
       askAPIforNewData();
    }
  }
  
  public void makeUpdatedMove(String firstPos, String secondPos){
    
    
    for(int row = 0; row < chessNotation.length; row++){
      for(int col = 0; col < chessNotation[row].length; col++){
        //println("checking if: " + chessNotation[row][col] + ", is equivelent to: " + firstPos );
        //println("checking if: " + chessNotation[row][col] + ", is equivelent to: " + secondPos );
        //println();
        if(chessNotation[row][col].equals(firstPos)){
          fromPosition[0] = row+1;
          fromPosition[1] = col+1;
          println("From posision: " + (row+1) + ", " + (col+1));
          chess.chessBoard.update(fromPosition[1]-1, fromPosition[0]-1, 6);
        }
        if(chessNotation[row][col].equals(secondPos)){
          toPosition[0] = row+1;
          toPosition[1] = col+1;
          println("To posision: " + toPosition[0] + ", " + toPosition[1]);
          chess.chessBoard.update(toPosition[1]-1, toPosition[0]-1, 5);
        }
      }
    }
  }
}
