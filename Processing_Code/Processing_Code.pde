import processing.serial.*;
import java.util.Map;

ApiCaller apiCall;

Serial myPort;

int boardWidth = 8;
int boardHeight = 8;
int[][] game;
int x = 0, y = 0;

int lastX = 0, lastY = 0;
int direction = 3;
Snake snake; // = new Snake(board);
Pong pong;
Chess chess;

String gameType = "snake";

int redV = 0;
int greenV = 0;
int blueV = 0;
int delayTime = 12;

boolean pressOnce = false;

int[][] ledPosition = {      {0, 1, 2, 3, 4, 5, 6, 7},
                             {15, 14, 13, 12, 11, 10, 9, 8},
                             {16, 17, 18, 19, 20, 21, 22, 23},
                             {31, 30, 29, 28, 27, 26, 25, 24},
                             {32, 33, 34, 35, 36, 37, 38, 39},
                             {47, 46, 45, 44, 43, 42, 41, 40},
                             {48, 49, 50, 51, 52, 53, 54, 55},
                             {63, 62, 61, 60, 59, 58, 57, 56},
                             }; //Pretty much used just to visulaize the array

String[][] chessNotation = {{"h8", "g8", "f8", "e8", "d8", "c8", "b8", "a8"},
                            {"h7", "g7", "f7", "e7", "d7", "c7", "b7", "a7"},
                            {"h6", "g6", "f6", "e6", "d6", "c6", "b6", "a6"},
                            {"h5", "g5", "f5", "e5", "d5", "c5", "b5", "a5"},
                            {"h4", "g4", "f4", "e4", "d4", "c4", "b4", "a4"},
                            {"h3", "g3", "f3", "e3", "d3", "c3", "b3", "a3"},
                            {"h2", "g2", "f2", "e2", "d2", "c2", "b2", "a2"},
                            {"h1", "g1", "f1", "e1", "d1", "c1", "b1", "1"}};

int count;
char messageBuffer[] = {0, 0, 0, 0, 0};
byte[] byteBuffer = new byte[7];

HashMap<String, Board> gameBoards = new HashMap();

String[] boardMoves = {"", ""};

//Chess Initialization
HashMap<String, ChessPiece> pieces = new HashMap();
String GameID;
boolean sendMove = false;
Thread thisThread;
MyThread getBoard;
String move = "";
ArrayList<String> pieceToDelete = new ArrayList();
 String[] moves = {"", ""};
 int[] fromPosition = {0, 0};
 int[] toPosition = {0, 0};
 String movingPiece = "";
  float[] lastPosition = new float[2];
 
 boolean yourTurn = true;

void setup(){
  size(800, 600);
  frameRate(60);
  stroke(0);
  strokeWeight(3);
  
  GameID = "RS4JFyka"; //This is the game ID for the Lichess game
  
  
   getBoard = new MyThread(GameID);
  thisThread = new Thread(getBoard);
  thisThread.start();
  
  //Securing Bluetooth Connection
  myPort = new Serial(this,"COM8", 38400);
  if(myPort.available() > 0){
    byteBuffer = myPort.readBytes();
    if(byteBuffer != null){
     //Clearing Connection buffer
    }
  }
  
  gameBoards.put("snake", new Board(boardHeight, boardWidth));
  gameBoards.put("pong", new Board(boardHeight, boardWidth));
  gameBoards.put("chess", new Board(boardHeight, boardWidth));
  
  
  myPort.write("-2/");
  
  for(int count = 0; count < 50; count++){ //Update matrix Screen
     myPort.write(count+"/"+redV+"/"+greenV+"/"+blueV+"/");
     //println(count+"/"+redV+"/"+greenV+"/"+blueV+"/");
     delay(3);
   }
  delay(10);
   myPort.write("-1/");
  
  //initializing games

  pong = new Pong(gameBoards.get("pong"));
  
  snake = new Snake(gameBoards.get("snake"));
  
  chess = new Chess(gameBoards.get("chess"));
 
  initializePieces();
 
}

void draw(){
  
  
  while(myPort.available() > 2){
    byteBuffer = myPort.readBytes(); 
    if(byteBuffer != null){
      String data = new String(byteBuffer);
      if(gameType == "pong"){
        parsePongData(data);
      }
      
      if(gameType == "chess"){
        parseChessData(data);
        //pick up piece "Select" --> pick up a captured piece
        //Put down piece to move to position
        
      }
      
    }
  }
  
  
    Board tempBoard = gameBoards.get(gameType);
    game = tempBoard.getBoard();
  
  
  
  if(gameType == "snake"){
    playSnake();
  }else if(gameType == "pong"){
    playPong();
  }else if(gameType == "chess"){
    playChess();
  }
  
  //Displaying game board
 
  if(frameCount % delayTime == 0){
   
    
    for(int row = 0; row < game.length; row++){
        for(int col = 0; col < game[row].length; col++){
          if(game[row][col] == 1){
             setColor(42, 9, 9); //green 0,255, 255 blue 19, 65, 232 pink  194, 27, 27 OR 232 19 164
          }else if (game[row][col] == 2){
            setColor(0, 42, 0); //red 255,0,0 orange: 255, 60, 5 purple 86, 19, 232
          }else if (game[row][col] == 3){
            setColor(0, 0, 0);
          }
          else if(game[row][col] == 5){
            setColor(0, 123, 123);
            println("Sending BLUE color -------");
          }else if(game[row][col] == 6){
            setColor(128, 0, 128);
          }else if(game[row][col] == 7){
            setColor(255, 255, 255);
          }else if(game[row][col] == 8){
            setColor(128, 68, 68);
          }
          int pos = game[row][col];
          if(pos != 4 && pos != 0){ 
          
              count = ledPosition[row][col];
              myPort.write("-2/");
              delay(10);
         
              //Update matrix Screen
              myPort.write(count+"/"+redV+"/"+greenV+"/"+blueV+"/");
           
              delay(3);
           
              if(pos == 1 || pos == 5 || pos == 2){
                 gameBoards.get(gameType).update(col, row, 4);
               }else if(pos == 3){
                  gameBoards.get(gameType).update(col, row, 0);
               }
            }
        
        }
    }
    
     delay(10);
     myPort.write("-1/");
  }
  
  
  
  
}


void setColor(int r, int g, int b){
  redV = r;
  greenV = g;
  blueV = b;
} 

void playSnake(){
 if(keyPressed){ //&& !pressOnce){
  //  pressOnce = true;
    if((key == 'w' || key == 'W') && direction != 2 ){
      //snake.moveUp();
      direction = 0;
    }
     if((key == 'a' || key == 'A') && direction != 3 ){
      //snake.moveLeft();
      direction = 3;
    }
     if((key == 's' || key == 'S') && direction != 0){
      //snake.moveDown();
      direction = 2;
    }
     if((key == 'd' || key == 'D') && direction != 1 ){
      //snake.moveRight();
      direction = 1;
    }
  }
  
  
   if(frameCount % 12 == 0 && !snake.dead){
     
        switch(direction){
          case 0: 
            snake.moveUp();
            break;
          case 1:
            snake.moveLeft();
            break;
          case 2:
           snake.moveDown();
            break;
          case 3:
            snake.moveRight();
            break;
        }
     
  }else if(frameCount % 8 == 0 && snake.dead){
     snake.deathAnimation();
  }
 
}

void playPong(){
  if(frameCount % pong.ballSpeed == 0){
    pong.drawBall();
  }
  
  
}

void playChess(){
    
    if(sendMove){
     PostRequest post = new PostRequest("https://lichess.org/api/board/game/"+ GameID +"/move/" + move);
     post.addHeader("Authorization","Bearer YlslUaE0uXn1stzS");
     post.send();
     
     delay(6000);//wait 2 seconds
      sendMove = false;
    println("");
    askAPIforNewData();

  }
  
  if(pieceToDelete.size() > 0){
    for(int count = pieceToDelete.size()-1; count >= 0; count--){
       pieces.remove(pieceToDelete.get(count));
    }
  }
  
  
}

void initializePieces(){
  pieces.put("WK", new King("W", 4, 8));
  pieces.put("WQ", new Queen("W", 5, 8));
  pieces.put("WB1", new Bishop("W", 3, 8));
  pieces.put("WB2", new Bishop("W", 6, 8));
  pieces.put("WR1", new Rook("W", 1, 8));
  pieces.put("WR2", new Rook("W", 8, 8));
  pieces.put("WKn1", new Knight("W", 2, 8));
  pieces.put("WKn2", new Knight("W", 7, 8));
  pieces.put("WP1", new Pawn("W", 1, 7));
  pieces.put("WP2", new Pawn("W", 2, 7));
  pieces.put("WP3", new Pawn("W", 3, 7));
  pieces.put("WP4", new Pawn("W", 4, 7));
  pieces.put("WP5", new Pawn("W", 5, 7));
  pieces.put("WP6", new Pawn("W", 6, 7));
  pieces.put("WP7", new Pawn("W", 7, 7));
  pieces.put("WP8", new Pawn("W", 8, 7));
  
  pieces.put("BK", new King("B", 4, 1));
  pieces.put("BQ", new Queen("B", 5, 1));
  pieces.put("BB1", new Bishop("B", 3, 1));
  pieces.put("BB2", new Bishop("B", 6, 1));
  pieces.put("BR1", new Rook("B", 1, 1));
  pieces.put("BR2", new Rook("B", 8, 1));
  pieces.put("BKn1", new Knight("B", 2, 1));
  pieces.put("BKn2", new Knight("B", 7, 1));
  pieces.put("BP1", new Pawn("B", 1, 2));
  pieces.put("BP2", new Pawn("B", 2, 2));
  pieces.put("BP3", new Pawn("B", 3, 2));
  pieces.put("BP4", new Pawn("B", 4, 2));
  pieces.put("BP5", new Pawn("B", 5, 2));
  pieces.put("BP6", new Pawn("B", 6, 2));
  pieces.put("BP7", new Pawn("B", 7, 2));
  pieces.put("BP8", new Pawn("B", 8, 2));
  
  
}


void stop(){
 myPort.write("-3/0/"); 
}


void keyPressed(){
  if(key == 'p' || key == 'P'){
    println("Changing game mode to pong");
    delayTime = 4;
    myPort.write("-2/");
    delay(100);
    myPort.write("-3/");
    
    gameType = "pong";
  }
  
  
    if (key == 'q' || key == 'Q'){
      println("Changing game mode to other");
    myPort.write("-2/");
    delay(100);
    myPort.write("-4/");
    //gameType = "else";
    }
    
    if(key == 'o' || key == 'O'){
      println("Changing game mode to snake");
      delayTime = 12;
    myPort.write("-2/");
    delay(100);
    myPort.write("-4/");
    gameType = "snake";
    //changeGame();
  }
  if(key == 'k' || key == 'K'){
    println("Changing game mode to chess");
    myPort.write("-2/");
    delay(100);
    myPort.write("-5/");
    gameType = "chess";
  }
  
  
}

void changeGame(){
  println("Switching game board");
  Board tempBoard = gameBoards.get(gameType);
  int[][] tempGame = tempBoard.getBoard();
  myPort.write("-2/");
  delay(100);
  for(int row = 0; row < tempGame.length; row++){
      for(int col = 0; col < tempGame[row].length; col++){
          if(tempGame[row][col] == 4){
             setColor(0, 255, 0);
          }else if (tempGame[row][col] == 2){
             setColor(255, 0, 0);
          }else if (tempGame[row][col] == 0){
              setColor(0, 0, 0);
          }
          int pos = tempGame[row][col];
          if(pos == 4 || pos == 2 || pos == 0){
         
         myPort.write(ledPosition[row][col]+"/"+redV+"/"+greenV+"/"+blueV+"/");
         delay(3);
          }
      }
  }
  delay(100);
  myPort.write("-1/");
}

void parsePongData(String data){
 String[] newPositions = splitTokens(data, ","); 
    if(newPositions.length>3){
        if(newPositions[1].equals("B")){
          println("Moving Paddel B");
          pong.paddelOne = parseInt(newPositions[0]); 
          println(pong.paddelOne + "B should be: " +  parseInt(newPositions[0]));
          
        }
        
        if( newPositions[3].equals("A")){
          println("Moving Paddel A");
          pong.paddelTwo = parseInt(newPositions[2]); 
          println(pong.paddelTwo + "A should be: " +  parseInt(newPositions[2]));
        }
        pong.updatePaddels();
        
    } 
}

void parseChessData(String data){
  String[] moveData = splitTokens(data, ":");
  println("data is: " + data);
    if(moveData[0].equals("u")){
      println("moveFrom: " + moveData[1]);
      boardMoves[0] = moveData[1];
    }
    
    if(moveData[0].equals("d")){
      println("moveTo: "+ moveData[1]);
      boardMoves[1] = moveData[1];
      println("player moved: " + boardMoves[0] + boardMoves[1]);
      
      for(Map.Entry piece: pieces.entrySet()){
        ChessPiece tempPiece = (ChessPiece)piece.getValue();
        if(!yourTurn){
          tempPiece.checkOpponentsMove(boardMoves[0], boardMoves[1]);
        }else{
        tempPiece.checkBoardMove(boardMoves[0], boardMoves[1]);
        }
      }
  }
}

void askAPIforNewData(){
    thisThread.interrupt();
    thisThread = new Thread(getBoard);
    thisThread.start();
    delay(3000);
    println("updating board..");
    //println(getBoard.getBoardState()); 
    getBoard.getResponse(); 
}
