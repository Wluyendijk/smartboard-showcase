class Pong{
 int ballX = 4, ballY = 2, xVelocity = 0, yVelocity = 1;
 int ballSpeed = 40, ballReset = ballSpeed;
 int paddelOne = 4, paddelTwo = 4;
 int oldPaddelOne = 0, oldPaddelTwo = 0;
 boolean lost = false;
 Board pongBoard;
 int loseDelay = 0, loseDelayTime = 5;
 int paddelOneColor = 1;
 int paddelTwoColor = 1;
 
 Pong(Board board){
   pongBoard = board;
 }
 
 void update(int paddelOne, int paddelTwo){
   this.paddelOne = paddelOne;
   this.paddelTwo = paddelTwo;
   drawBall();
   updatePaddels();
 }
 
 void updatePaddels(){
   if(paddelOne != oldPaddelOne){
     removeOldPaddel(oldPaddelOne, 0);
     drawPaddel(paddelOne, 0, paddelOneColor);
     oldPaddelOne = paddelOne;
   }
   
   if(paddelTwo != oldPaddelTwo){
     removeOldPaddel(oldPaddelTwo, 7);
     drawPaddel(paddelTwo, 7, paddelTwoColor);
     oldPaddelTwo = paddelTwo;
   }
 }
 
 void drawPaddel(int paddelPosition, int row, int paddelColor){
   if(paddelPosition-1 >= 0){ pongBoard.update(paddelPosition-1, row, paddelColor);}
   pongBoard.update(paddelPosition, row, paddelColor);
   if(paddelPosition+1 < boardWidth){ pongBoard.update(paddelPosition+1, row, paddelColor);}
 }
 
 void removeOldPaddel(int paddelPosition, int row){
    if(paddelPosition-1 >= 0) pongBoard.update(paddelPosition-1, row, 3);
    pongBoard.update(paddelPosition, row, 3);
    if(paddelPosition+1 < boardWidth) pongBoard.update(paddelPosition+1, row, 3);
 }
 
 void drawBall(){
   
   if(!lost){
      pongBoard.update(ballX, ballY, 3);
      ballX = ballX + xVelocity;
      if(ballX == pongBoard.boardWidth-1){
        xVelocity = -1;
      }else if(ballX == 0){
        xVelocity = 1;
      }
      
      //check if ball is eith hit the paddel or gone past
      ballY = ballY + yVelocity;
      
      if(ballY == pongBoard.boardHeight-2){
        checkPaddelCollision(paddelTwo, -1);
         pongBoard.update(ballX, ballY, 2);
      }else if(ballY == 1){
        checkPaddelCollision(paddelOne, 1);
         pongBoard.update(ballX, ballY, 2);
      }else if(ballY == pongBoard.boardHeight){
          paddelOneColor = 5;
          paddelTwoColor = 2;
          lost = true;
          ballSpeed = ballReset;
          ballY = 3;
          ballX = 4;
          yVelocity = -1;
          pongBoard.update(ballX, ballY, 2);
          drawPaddel(paddelOne, 0, paddelOneColor);
          drawPaddel(paddelTwo, 7, paddelTwoColor);
          println("changed paddel One color to: " + paddelOneColor);
         
        
      }else if(ballY < 0){
          paddelTwoColor = 5;
          paddelOneColor = 2;
          lost = true;
          ballSpeed = ballReset;
          ballY = 3;
          ballX = 4;
          yVelocity = 1;
          pongBoard.update(ballX, ballY, 2);
          drawPaddel(paddelTwo, 7, paddelTwoColor);
          drawPaddel(paddelOne, 0, paddelOneColor);
          println("Changed paddel Two color to: " + paddelTwoColor);
          
        
      }else{
        pongBoard.update(ballX, ballY, 2);
      }
   }else{
      loseDelay(); 
   }
 }
 
 void checkPaddelCollision(int paddel, int newVelocity){
    
   if(ballX == paddel){
     this.xVelocity = 0;
     yVelocity = newVelocity;
     increaseBallSpeed();
   }else if(ballX == paddel - 1){
     if(ballX == 0){
       xVelocity = 1;
     }else{
      xVelocity = -1; 
     }
     yVelocity =  newVelocity;
     increaseBallSpeed();
   }else if(ballX == paddel + 1){
     if(ballX == 7){
       xVelocity = -1;
     }else {
       xVelocity = 1;
     }
     yVelocity = newVelocity;
     increaseBallSpeed();
   }
 }
 
 void increaseBallSpeed(){
   if(ballSpeed > 10){
      ballSpeed = ballSpeed - 2;
      println("Ball speed is: " + ballSpeed);
   }else{
      ballSpeed = 10; 
      println("Ball speed is: " + ballSpeed);
   }
 }
 
 void loseDelay(){
   if(loseDelay < loseDelayTime){
     loseDelay++;
     println("Game reset time is: " + loseDelay);
   }else{
     loseDelay = 0;
     lost = false;
     paddelOneColor = 1;
     paddelTwoColor = 1;
     drawPaddel(paddelOne, 0, paddelOneColor);
     drawPaddel(paddelTwo, 7, paddelTwoColor);
     
   }
 }

}
