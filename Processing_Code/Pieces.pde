class King extends ChessPiece{
 boolean selected = false;
 
  King(String pieceColor, float row, float col){
      super(pieceColor, row, col, "king");
     
  }
  
  void checkValidMove(int col, int row){
         if(row >= position[0]-1 && row <= position[0]+1 && col >= position[1]-1 && col <= position[1]+1){
               movePiece(row,col);
           }else if(row == position[0] && col == position[1]){
               selected = false;
           }
   }

}

class Queen extends ChessPiece{
  
   boolean selected = false;
   float difX;
   float difY;
   
  Queen(String pieceColor, float row, float col){
    super(pieceColor, row, col, "Quen");
  }
  
  void checkValidMove(int col, int row){
    //move liniarly 
        if((row == position[0] && col != position[1]) || (row != position[0] && col == position[1]) ){
         
              movePiece(row, col);
          
        }else{
        
          difX = col - position[0];
          difY = row - position[1];
          
          //move diagonally
          if(difX == difY || difX == -difY || -difX == difY){
                movePiece(row, col);
            
          }
      }
  }
  
  boolean checkLineirPath(int row, int col){
    boolean response = false;
    
    return response;
  }
  
 
  
}

class Bishop extends ChessPiece{
  boolean selected = false; 
  float difX;
  float difY;
  
  Bishop(String pieceColor, int row, int col){
    super(pieceColor, row, col, "  B");
  }
  
  void checkValidMove(int col, int row){
        difX = col - position[0];
        difY = row - position[1];
        if(difX == difY || difX == -difY || -difX == difY){
          movePiece(row, col);
        }
  }
  
}

class Rook extends ChessPiece{
 
  boolean selected = false;
  
  Rook(String pieceColor, int row, int col){
    super(pieceColor, row, col, "Rook");
  }
  
  void checkValidMove(int col, int row){
        if((row == position[0] && col != position[1]) || (row != position[0] && col == position[1]) ){
           movePiece(row, col);
         } 
  }
  
}

class Knight extends ChessPiece{
   boolean selected = false;
 
   Knight(String pieceColor, int row, int col){
     super(pieceColor, row, col, " Kn");
   }
   
   void checkValidMove(int col, int row){
          if(position[0]+2 == row && (position[1]+1 == col || position[1]-1 == col)){
             movePiece(row, col);
          }else if(position[0]-2 == row && (position[1]+1 == col || position[1]-1 == col)){ //check if it's moved left
             movePiece(row, col);
          }else if(position[1]+2 == col && (position[0]+1 == row || position[0]-1 == row)){//check if it's moved up
             movePiece(row, col);
          }else if(position[1]-2 == col && (position[0]+1 == row || position[0]-1 == row)){
             movePiece(row, col);
          }
   }
}

class Pawn extends ChessPiece{
   boolean selected = false;
   boolean firstMove = true;
   int farthestMove;
   int moveValue;
   
   Pawn(String pieceColor, int row, int col){
     super(pieceColor, row, col, "  P");
     if(pieceColor == "W"){
       farthestMove = -2;
       moveValue = -1;
     }else{
       farthestMove = 2;
       moveValue = 1;
     }
   }
   
   void checkValidMove(int col, int row){
     
       println(col + " = " +  (position[1]+moveValue) +  " && " + position[0] + " == " + row);
       if(firstMove){
         if(col == position[1]+farthestMove && position[0] == row){
            println("Valid MOVE");
           movePiece(row, col);
           firstMove = false;
         }else if(col == position[1]+moveValue && (position[0] == row || position[0]+1 == row || position[0]-1 == row)){
            println("Valid MOVE");
           movePiece(row, col);
           firstMove = false;
         }
       }
       if(col == position[1]+moveValue && (position[0] == row || position[0]+1 == row || position[0]-1 == row)){
          println("Valid MOVE");
         movePiece(row, col);
       }
     
     
     
   }
  
}
