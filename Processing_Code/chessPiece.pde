class ChessPiece{
 String pieceColor;
 float[] position = new float[2];
 String pieceType;
 int associatedType;
 boolean selected = false;
 int[][] chessBoard;

 
 ChessPiece(String pieceColor, float row, float col, String pieceType){
    this.pieceColor = pieceColor;
    position[0] = row;
    position[1] = col;
    lastPosition[0] = position[0];
    lastPosition[1] = position[1];
    this.pieceType = pieceType;
    
    if(pieceColor == "W"){
      associatedType = 2; //3 if you don't want the chess pieces to display a color
    }else{
      associatedType = 1; //3 if you don't want the chess pieces to display a color
    }
    
    chessBoard = chess.chessBoard.getBoard();
 }
 
 void drawPiece(){
   if(pieceColor == "W"){
      fill(255, 0, 0);
   }else if(pieceColor == "B"){
      fill(0);
   }
   if(selected){
      fill(0, 255, 0); 
   }
   text(pieceType, 0+(position[0]*(width/8))-85, 0+(position[1]*(height/8))-40);
 }
 
 void checkBoardMove(String moveFrom, String moveTo){
   
     boolean verifiedPiece = checkValid(moveFrom);
     if(pieceColor == "W" && yourTurn){
         if(verifiedPiece){
           int xPos = 0, yPos = 0;
           for(int row = 0; row < chessNotation.length; row++){
             for(int col = 0; col < chessNotation[row].length; col++){
               if(chessNotation[row][col].equals(moveTo)){
                 println("Found Move to Position");
                 xPos = row+1;
                 yPos = col+1;
                 moves[0] = moveFrom;
                 checkValidMove(xPos, yPos);
                 movePiece(yPos, xPos);
                 break;
               }
             }
           }
       }
     }
   
 }
 
void checkOpponentsMove(String moveFrom, String moveTo){
        boolean verifiedPiece = checkValid(moveFrom);  
        
        if(verifiedPiece){
          
             int xPos = 0, yPos = 0;
             for(int row = 0; row < chessNotation.length; row++){
               for(int col = 0; col < chessNotation[row].length; col++){
                 if(chessNotation[row][col].equals(moveTo)){
                   xPos = row+1;
                   yPos = col+1;
                   println("Oppnonets move coordinates is: " + xPos + ", " + yPos);
                 }
               }
             }
             println(toPosition[0] + " == " + xPos + " && " + toPosition[1] + " == " + yPos);
             if(toPosition[0] == xPos && toPosition[1] == yPos){
               println("To position is correct");
                if((int)position[1] == fromPosition[0] && (int)position[0] == fromPosition[1]){
                  println("MOVING");
                  checkRemovePiece(toPosition[1], toPosition[0]);
                  
                  position[0] = toPosition[1];
                  position[1] = toPosition[0];
                  
                  //chess[(int)fromPosition[0]-1][(int)fromPosition[1]-1] = 0;
                  chess.chessBoard.update((int)fromPosition[1]-1, (int)fromPosition[0]-1, 3);
                  delay(100);
                  //chess[(int)toPosition[0]-1][(int)toPosition[1]-1] = associatedType;
                  chess.chessBoard.update((int)toPosition[1]-1,(int)toPosition[0]-1, associatedType);
                  //turn off led colors
                  //board.update(toPosition[1]-1, toPosition[0]-1, 0);
                  //board.update(fromPosition[1]-1, fromPosition[0]-1, 0);
                  println("WHITE'S TURN");
                  yourTurn = true;
                  
                }
          }
        }
             
       
 }
 
 void movePiece(int row, int col){
   
   println("MOVING A CHESS PIECE");
     //checkRemovePiece(row, col);
     
     //chess[(int)position[1]-1][(int)position[0]-1] = 0;
    
     
     
     lastPosition[0] = position[0];
     lastPosition[1] = position[1];
     
     position[0] = row;
     position[1] = col;
     moves[1] = chessNotation[col-1][row-1];
     
      chess.chessBoard.update((int)lastPosition[0]-1,(int)lastPosition[1]-1,3);
     //chess[col-1][row-1] = associatedType; 
     chess.chessBoard.update((int)position[0]-1,(int)position[1]-1, associatedType);
     
     move = moves[0] + moves[1];
     println(move);
     sendMove = true;
     yourTurn = false;
 
 }
 
 boolean checkGridForPiece(int row, int col){
   boolean response = true;
   
   if(chessBoard[col-1][row-1] == associatedType){
     response = false;
   }
   
   return response;
 }
 
void checkValidMove(int row, int col){
    // This is here to be over writen by each piece base on their move type
 }
 
 void showMoves(int highLight){
   // This is here to be over writen by each piece base on their move type
 }
 
 boolean checkBlockedPath(int row, int col){
   boolean response = true;
   
   //do something here
   
   return response;
 }
 
 void checkRemovePiece(int row, int col){
    for(Map.Entry piece: pieces.entrySet()){
      ChessPiece tempPiece = (ChessPiece)piece.getValue();
      
      //println((int)tempPiece.position[1] + " == " + col + " col");
      //println((int)tempPiece.position[0] + " == " + row + ": row");
       if((int)tempPiece.position[1] == col && (int)tempPiece.position[0] == row){
         println("FOUND MATCH");
         //String code = piece.getKey();
         pieceToDelete.add((String)piece.getKey());
         //chess[(int)tempPiece.position[0]-1][(int)tempPiece.position[1]-1] = 0;
         chess.chessBoard.update((int)tempPiece.position[1]-1,(int)tempPiece.position[0]-1,3);
         //pieces.remove(piece.getValue());
         //pieces.remove(piece.getKey());
         //break;
       }
    }
 }
 
 boolean checkValid(String moveFrom){
   boolean verifiedPiece = false;
    for(int row = 0; row < chessNotation.length; row++){
         for(int col = 0; col < chessNotation[row].length; col++){
           if(chessNotation[row][col].equals(moveFrom)){
            // println("found from chess notation: " + chessNotation[row][col] + " at " + (row+1) + " & " + (col+1));
              if(row+1 == position[1] && col+1 == position[0]){
                verifiedPiece = true;
                println("found Piece");
              }
              break;
           }
         }
       }
     
     return verifiedPiece;
   
 }
 
 
 
}
