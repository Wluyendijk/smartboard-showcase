class Board{
 
  int[][] board;
  int boardWidth;
  int boardHeight;
  
  Board(int size1, int size2){
    boardWidth = size2;
    boardHeight = size1;
    board = new int[size1][size2];
    //Generate board 
    for(int row = 0; row < size1; row++){
      for(int col = 0; col < size2; col++){
        board[row][col] = 0;
      }
    }
  }
  
  int[][] getBoard(){
     return board; 
  }
  
  void setBoard(int[][] newSet){
    board = newSet;
  }
  
  void update(int x, int y, int value){
    board[y][x] = value;
  }
  
  
}
