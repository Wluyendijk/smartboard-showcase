/**
 * Author: Wim Luyendijk
 * Student Number: 445662
 * last Updated: 1/19/2021
 */

//Initializing Library for Shift Registers
#include <SPI.h>
//MOSI: pin ll don't use for anything
//MISO: pin 12 to Q7 of last register
//SCK: pin 13 to clock pin
//PL: any pin (pin 2)

//Initializing Library for LED matrix
#include<FastLED.h>
#define LED_PIN 3
#define NUM_LEDS 64
CRGB leds[NUM_LEDS];

//Initializing shift register data
byte shift_1, shift_2, shift_3, shift_4, shift_5, shift_6, shift_7, shift_8;
int getData = 2; //PL for SPI Library

//Initializing Bluetooth data Buffers
char messageBuffer[] = {0, 0, 0, 0, 0} ;
char redBuffer[] = {0, 0, 0, 0, 0};
char greenBuffer[] = {0, 0, 0, 0, 0} ;
char blueBuffer[] = {0, 0, 0, 0, 0} ;
size_t bytesRecieved;
String x;
int redV = 0, greenV = 0, blueV = 0;
int currentPosition = 0;
int BufferSize = 5;

//Declaring gameBoard
String gameMode = "snake";
int xPosition;
int yPosition;
int boardLength = 8;

//Initializing Pong paddels
String paddelOne = String("");
String paddelTwo = String("");

boolean placePiece = false;

void setup() {
  //Initializing Shift Registers
   initializeShiftRegisters(); //Initializations
 
  //Initializing LED Matrix
   initializeLEDMatrix();      //Initializations

  //Setting up Bluetooth connection
  Serial.begin(38400);

}

void loop() {
//  if(gameMode == "pong"){
//    getSensorData();
//  }
  
  //Arduino Bluetooth Serial communication: PRGB: Position, Red, Gree, Blue 
  if(Serial.available() > 0){
    readSerialData();          //ReadSerial
  }else if(gameMode == "pong"){
    getSensorData();
    sendPongData();            //Pong
  }else if(gameMode == "chess"){
    getSensorData();
    walkThroughSensorData();   //Chess
  }
  
}

