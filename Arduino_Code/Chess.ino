
void walkThroughSensorData(){
  //Loop through all of the sensors here
  for(int count = 0; count < NUM_LEDS; count++){
    //Checking the first shift register for magnet detection
    if(count >= 0 && count < 8){  
      updateLED(shift_1, count, count);                        //Board
    }
    //Checking the second shift register for magnet detection
    if(count >= 8 && count < 16){
      updateLED(shift_2, InvertLEDdirection(count) , count);   //Board
    }
    //Checking the third shift register for magnet detection
    if(count >= 16 && count < 24){
      updateLED(shift_3, (int)(count-16), count);              //Board
    }
    //Checking the fourth shift register for magnet detection
    if(count >= 24 && count < 32){
      updateLED(shift_4, InvertLEDdirection(count), count);    //Board
    }
    //Checking the fifth shift register for magnet detection
    if(count >= 32 && count < 40){
        updateLED(shift_5, count-32, count);                   //Board
    }
    //Checking the sixth shift register for magnet detection
    if(count >= 40 && count < 48){
      updateLED(shift_6, InvertLEDdirection(count), count);    //Board
    }
    //Checking the seventh shift register for magnet detection
    if(count >= 48 && count < 56){
      updateLED(shift_7, count-48, count);                      //Board
    }
    //Checking the eighth shift register for magnet detection
    if(count >= 56 && count <64 ){
      updateLED(shift_8, InvertLEDdirection(count), count);     //Board
    }
  }

}

/*****
 * InvertLEDdirection
 * this function is used by walkThroughSensorData(), on rows where the LEDs
 * are installed in the opposite direction. On the board the LED string moves from 
 * right to left for each row, but the Sensor PCBs are all in the same direction.
 *****/
int InvertLEDdirection(int countPosition){
  //Inverting Sensor position to match LED position
  return (boardLength-((countPosition+1)%boardLength))%boardLength;
}

