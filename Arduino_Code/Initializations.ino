void initializeShiftRegisters(){
  SPI.setClockDivider(SPI_CLOCK_DIV2);
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);
  SPI.begin();

  pinMode(getData, OUTPUT);
  digitalWrite(2, HIGH);
}

void initializeLEDMatrix(){
  FastLED.addLeds<WS2812, LED_PIN, RGB>(leds, NUM_LEDS);
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 500);
  //FastLED.setBrightness(50);
  FastLED.clear();
  FastLED.show();
}
