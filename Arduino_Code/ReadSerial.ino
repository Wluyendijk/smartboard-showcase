void readSerialData(){ 
  //EX: 32/000/255/000/ EX2: -1/
     bytesRecieved = Serial.readBytesUntil('/',messageBuffer, BufferSize);
     currentPosition = atoi(messageBuffer);
     if(currentPosition == -1){
        FastLED.show();
        //updateBoard();                   //Board      
     }else if(currentPosition == -2){
        clearBuffers();               
     }else if(currentPosition == -3){
        gameMode = "pong";
     }else if(currentPosition == -4){
        gameMode = "snake";
     }else if(currentPosition == -5){
        gameMode = "chess";
     }else{
        setLEDColor();   //Board
     }
}


void clearBuffers(){
   memset( messageBuffer, 0, sizeof(messageBuffer));
   memset( redBuffer, 0, sizeof(redBuffer));
   memset( greenBuffer, 0, sizeof(greenBuffer));
   memset( blueBuffer, 0, sizeof(blueBuffer));
}
