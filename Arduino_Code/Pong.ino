void sendPongData(){
   boolean message = false;
    int valueOne;
    int valueTwo;
  
    for(int count = 0; count < 8; count++){
        valueOne = bitRead(shift_1, count);
        valueTwo = bitRead(shift_8, count);
        if(valueOne == 0 || valueTwo == 0){
           message = sendPaddelPosition(count, valueOne, valueTwo, message);
        }
    }

  if(message){
    //Sending data to the Processing program over Serial Connection
    Serial.println(paddelOne+","+paddelTwo);
  }
}


boolean sendPaddelPosition(int count, int v1, int v2, boolean message){
   String xPos = String(count);
   
   if(!paddelOne.equals(xPos+",B") && v1 == 0 ){
        paddelOne = xPos+",B";
        message = true;
   }

   if(!paddelTwo.equals(xPos+",A,") && v2 == 0){
        paddelTwo = xPos+",A,";
        message = true;
   }

   return message;
}
