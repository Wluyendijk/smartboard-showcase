
String chessNotation[] = {"h8", "g8", "f8", "e8", "d8", "c8", "b8", "a8",
                          "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
                          "h6", "g6", "f6", "e6", "d6", "c6", "b6", "a6",
                          "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
                          "h4", "g4", "f4", "e4", "d4", "c4", "b4", "a4",
                          "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
                          "h2", "g2", "f2", "e2", "d2", "c2", "b2", "a2",
                          "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"};

int piecePlaces[] = {0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, 0, 
                     0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, 0};



/*****
 * This function is used by arduino to update position via Sensors
 *****/
void updateLED(byte shift, int shiftPos, int ledPos){
  if(bitRead(shift, shiftPos) == 0 ){ // && leds[ledPos] == CRGB(0, 0, 0)){
     
     
     if(placePiece && piecePlaces[ledPos] == 0){
      leds[ledPos] = CRGB(0, 123, 123);
        String dataToSend = "d:" + chessNotation[ledPos];
        delay(100);
        Serial.print(dataToSend);
        placePiece = false;
     }//else{
      piecePlaces[ledPos] = 1;
     //}
  }else if(bitRead(shift, shiftPos) == 1 ){ // && leds[ledPos] != CRGB(0, 0, 0)){
     
     if(!placePiece && piecePlaces[ledPos] == 1){
        leds[ledPos] = CRGB(0, 0, 0);
        piecePlaces[ledPos] = 0; 
        String dataToSend = "u:" + chessNotation[ledPos];
        delay(100);
        Serial.print(dataToSend);
        placePiece = true;
     }else if(piecePlaces[ledPos] != 0){
       piecePlaces[ledPos] = 0;
     }
  }
  
}

void setLEDColor(){
    bytesRecieved = Serial.readBytesUntil('/',redBuffer, BufferSize);
    redV = atoi(redBuffer);

    bytesRecieved = Serial.readBytesUntil('/',greenBuffer, BufferSize);
    greenV = atoi(greenBuffer);
    
    bytesRecieved = Serial.readBytesUntil('/',blueBuffer, BufferSize);
    blueV = atoi(blueBuffer);
    
    leds[currentPosition] = CRGB(redV, greenV, blueV);
}

void updateBoard(){
  FastLED.show();
}

void getSensorData(){
  //Take picture of all sensors
  digitalWrite(getData, LOW);
  delay(1);
  digitalWrite(getData, HIGH);

  //Get data from each sensor
  shift_8 = SPI.transfer(0x00);
  shift_7 = SPI.transfer(0x00);
  shift_6 = SPI.transfer(0x00);
  shift_5 = SPI.transfer(0x00);
  shift_4 = SPI.transfer(0x00);
  shift_3 = SPI.transfer(0x00);
  shift_2 = SPI.transfer(0x00);
  shift_1 = SPI.transfer(0x00);

  //FastLED.clear();
}


